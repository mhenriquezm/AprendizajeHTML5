//Variable global
var bd = {};

var main = () => {
	//Si omitimos la palabra reservada var la variable es global
	zonaDatos = document.getElementById("zonadatos");
	boton = document.getElementById("grabar");

	boton.addEventListener("click", agregarObjeto, false);

	//La palabra reservada var limita el ambito de la variable
	
	//Crear la BBDD con open y la almacenamos el recurso
	var solicitud = indexedDB.open("BBDD_peliculas");

	//Usamos el evento onsuccess (Si tiene éxito)
	solicitud.addEventListener("success", (e) => {
		//Si la BBDD fue creada almacenamos la BBDD en un objeto
		bd = e.target.result;
	}, false);

	//Verificamos si la BBDD necesita una actualización
	solicitud.addEventListener("upgradeneeded", (e) => {
		//Prioridad de threads (Este evento se ejecuta una vez)
		bd = e.target.result;
		//Creamos un almacén de datos (una tabla) con nombre y PK
		bd.createObjectStore("gente", {keyPath: "clave"});
		//Esto garantiza que la BBDD es creada una sola vez
	}, false);
};

var agregarObjeto = () => {
	//Rescatamos el valor de los imputs
	var clave = document.getElementById("clave");
	var titulo = document.getElementById("texto");
	var fecha = document.getElementById("fecha");

	//Creamos una transacción (informar al navegador la acción)
	
	/*Las acciones son las mismas de archivos:
		- Lectura
		- Escritura 
		- Lectura y escritura

	El método transaction pide dos argumentos, el almacén de datos 
	donde se realizará la acción entre corchetes y el tipo de 
	acción. Luego de crear la transacción almacenamos la acción
	de este modo al realizar futuras transacciones solo invocamos
	a la variable*/

	var transaccion = bd.transaction(["gente"], "readwrite");
	var almacen = transaccion.objectStore("gente");
	
	//Almacenamos dinamicamente los campos (objetos)
	var agregar = almacen.add({
		clave: clave.value,
		titulo: titulo.value,
		fecha: fecha.value
	});

	//Si tiene éxito el agregar un objeto llamamos a mostrar
	agregar.addEventListener("success", mostrar, false);

	//Reseteamos los campos
	clave.value = "";
	titulo.value = "";
	fecha.value = "";
};

var mostrar = () => {
	zonaDatos.innerHTML = "";

	//Creamos una transacción
	var transaccion = bd.transaction(["gente"], "readonly");
	var almacen = transaccion.objectStore("gente");

	//Creamos un cursor (resultset)
	var cursor = almacen.openCursor();

	cursor.addEventListener("success", mostrarDatos, false);
};

var mostrarDatos = (e) => {
	//Guardamos el evento, es decir, el cursor
	var cursor = e.target.result;

	//En caso de tener un cursor, recorremos sus registros
	if(cursor) {
		//Agregamos cada registro accediendo a los campos
		zonaDatos.innerHTML += "" 
			+ "<div>" 
				+ cursor.value.clave + " - "
				+ cursor.value.titulo + " - "
				+ cursor.value.fecha
			+ "</div>";

		//Avanzamos al siguiente registro
		cursor.continue();
	}
};

window.addEventListener("load", main, false);