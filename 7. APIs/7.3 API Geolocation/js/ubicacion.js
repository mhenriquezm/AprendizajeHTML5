var main = () => {
	var darUbicacion = document.getElementById('darUbicacion');

	darUbicacion.addEventListener('click', capturar, false);
};

var capturar = () => {
	if(navigator.geolocation) {
		var configuraciones = {
			enableHighAccuracy: true,//Activar ubicación GPS
			timeout: 10000,//Límite para obtener la localización
			maximumAge: 60000//Consulta la posición en la caché
		};

		//El navigator es el navegador y geolocation es un objeto
		navigator.geolocation.watchPosition(
			mostrarPosicion,//Parámetro obligatorio
			gestionErrores,//En caso de no capturar la posición
			configuraciones//Llamamos la posición cada minuto
		);	
	}
	else {
		alert('Su navegador no soporta la API Geolocation');
	}
	
};

//La función llamada por getCurrtentPosition recibe la posición
var mostrarPosicion = (posicion) => {
	var seccion = document.getElementById('ubicacion');

	//Creamos un objeto que guarde las coordenadas
	var datos = {
		latitud : 'Latitud: ' + posicion.coords.latitude,
		longitud : 'Longitud: ' + posicion.coords.longitude,
		exactitud : 'Exactitud: ' + posicion.coords.accuracy
	};

	//Mostramos la información
	seccion.innerHTML += datos.latitud
	 + '<br/>' + datos.longitud
	 + '<br/>' + datos.exactitud;
};

//El segundo parámetro devuelve un objeto de tipo error (excepción)
var gestionErrores = (error) => {

	switch(error.code) {
        case error.PERMISSION_DENIED:
        	alert("Ha denegado los permisos de ubicación. " 
        		+ error.message);
        break;
        case error.POSITION_UNAVAILABLE:
        	alert("Ubicación no disponible. " 
        		+ error.message);
        break;
        case error.TIMEOUT:
        	alert("Tiempo de respuesta agotado." 
        		+ error.message);
        break;
        case error.UNKNOWN_ERROR:
        	alert("Ha ocurrido un error inesperado." 
        		+ error.message);
        break;
    }
};

window.addEventListener('load', main, false);