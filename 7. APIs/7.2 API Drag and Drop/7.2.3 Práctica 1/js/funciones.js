function main() {
	var objOrigen = document.getElementById('imagen');
	var zonaDestino = document.getElementById('zonaDestino');

	objOrigen.addEventListener('dragstart', (e) => {
		inicioArrastre(e, objOrigen);
	}, false);

	objOrigen.addEventListener('dragend', (e) => {
		finArrastre(e);
	}, false);

	zonaDestino.addEventListener('dragenter', (e) => {
		entrar(e, zonaDestino);
	}, false);

	//Las funciones anónimas son utiles cuando el código es simple
	zonaDestino.addEventListener('dragover', (e) => {
		e.preventDefault();
	}, false);

	zonaDestino.addEventListener('dragleave', (e) => {
		salir(e, zonaDestino);
	}, false);

	//Si el código es complejo llamar a una función convencional
	zonaDestino.addEventListener('drop', (e) => {
		soltar(e, zonaDestino);
	}, false);
}

var inicioArrastre = (e, objOrigen) => {
	//Capturamos la ruta de la imágen actual
	var codigo = '<img src="'
		+ objOrigen.getAttribute('src')
		+ '"/>';

	//Establecemos la información a compartir
	e.dataTransfer.setData('Text', codigo);
};

var finArrastre = (e) => {
	//Capturamos el objeto desencadenante del evento
	var fuenteEvento = e.target;
	var mensaje = document.querySelector('#mensaje');

	if(mensaje == null) {
		//Lo hacemos invisible
		fuenteEvento.style.visibility = 'hidden';
	}

};

var entrar = (e, zonaDestino) => {
	e.preventDefault();
	zonaDestino.style.background = 'rgba(8, 252, 25, .3)';
};

var salir = (e, zonaDestino) => {
	e.preventDefault();
	zonaDestino.style.background = 'rgb(255, 255, 255)';
};

var soltar = (e, zonaDestino) => {
	//Prevenimos el comportamiento por defecto
	e.preventDefault();

	//Agregamos el código a la zona
	zonaDestino.innerHTML = e.dataTransfer.getData('Text');

	//Damos formato a la zona
	zonaDestino.style.background = '#fff';
};

window.addEventListener('load', main, false);