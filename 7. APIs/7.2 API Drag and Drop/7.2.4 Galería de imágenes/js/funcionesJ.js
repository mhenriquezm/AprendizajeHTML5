var zonaDestino;

function main() {
	var imagenes = document.querySelectorAll('#cajaImagen img');
	zonaDestino = document.getElementById('zonaDestino');
	var cantImagenes = imagenes.length;

	for(var i = 0; i < cantImagenes; i++) {
		imagenes[i].addEventListener('dragstart', inicioArrastre);

		if(i != 1) {
			imagenes[i].addEventListener('dragend', finArrastre);
		}
	}

	zonaDestino.addEventListener('dragenter', entrar, false);
	zonaDestino.addEventListener('dragleave', salir, false);
	zonaDestino.addEventListener('dragover', function(e) {
		e.preventDefault();
	}, false);
	zonaDestino.addEventListener('drop', soltarElemento, false);
}

function inicioArrastre(e) {
	var fuenteEvento = e.target;
	e.dataTransfer.setData('Text', fuenteEvento.id);
}

function finArrastre(e) {
	var fuenteEvento = e.target;
	fuenteEvento.style.visibility = 'hidden';
}

function entrar(e) {
	e.preventDefault();

	var id = e.dataTransfer.getData('Text');

	if(id != 'imagen2') {
		zonaDestino.style.background = "rgba(8, 252, 25, .8)";
	}
	else {
		zonaDestino.style.background = "#fa0d29";
	}
}

function salir(e) {
	e.preventDefault();
	zonaDestino.style.background = "#fff";
}

function soltarElemento(e) {
	e.preventDefault();

	var id = e.dataTransfer.getData('Text');

	if(id != 'imagen2') {
		var src = document.getElementById(id).src;

		zonaDestino.innerHTML = '<img src="' + src + '">';
	}
	else {
		zonaDestino.innerHTML = 'La imágen no es admitida';
		zonaDestino.style.background = '#fa0d29';
	}
}

window.addEventListener('load', main, false);