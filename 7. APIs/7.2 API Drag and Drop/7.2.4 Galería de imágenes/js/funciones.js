var imgAnterior = null;
var salio = false;

var main = () => {
	var selectorImg = '#caja-imagenes article figure img';
	var imagenes = document.querySelectorAll(selectorImg);
	var zonaDestino = document.getElementById('zona-destino');
	var cantImagenes = imagenes.length;

	for(var i = 0; i < cantImagenes; i++) {
		imagenes[i].addEventListener('dragstart', (e) => {
			inicioArrastre(e);
		}, false);

		imagenes[i].addEventListener('dragend', (e) => {
			finArrastre(e);
		}, false);
	}

	zonaDestino.addEventListener('dragenter', (e) => {
		entrar(e, zonaDestino);
	}, false);

	zonaDestino.addEventListener('dragleave', (e) => {
		salir(e, zonaDestino);
	}, false);

	zonaDestino.addEventListener('dragover', (e) => {
		e.preventDefault();
	}, false);

	zonaDestino.addEventListener('drop', (e) => {
		soltarElemento(e, zonaDestino);
	}, false);
};

var inicioArrastre = (e) => {
	var fuenteEvento = e.target;
	e.dataTransfer.setData('Text', fuenteEvento.id);
};

var finArrastre = (e) => {
	var fuenteEvento = e.target;
	var mensaje = document.querySelector('#mensaje');

	if(
		(fuenteEvento.id == 'imagen1')
		|| (fuenteEvento.id == 'imagen2')
		|| (fuenteEvento.id == 'imagen3')
		|| (fuenteEvento.id == 'imagen4')
	) {
		if(mensaje == null) {
			if(!salio) {
				fuenteEvento.style.visibility = 'hidden';

				if((imgAnterior != null) 
					&& (imgAnterior.id != fuenteEvento.id)) {

					imgAnterior.style.visibility = 'visible';
				}

				imgAnterior = fuenteEvento;
			}
		}
	}
};

var entrar = (e, zonaDestino) => {
	e.preventDefault();
	zonaDestino.style.background = "#e4e4e4";
	salio = false;
};

var salir = (e, zonaDestino) => {
	e.preventDefault();
	zonaDestino.style.background = "#fff";
	salio = true;
};

var soltarElemento = (e, zonaDestino) => {
	e.preventDefault();

	var id = e.dataTransfer.getData('Text');
	var src = document.getElementById(id).src;

	zonaDestino.innerHTML = '<img src="' + src + '">';
	zonaDestino.style.background = "#fff";
};

window.addEventListener('load', main, false);