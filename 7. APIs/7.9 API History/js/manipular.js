var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	url = document.getElementById("url");

	url.addEventListener("click", cambiarUrl, false);

	//Al cambiar la url
	window.addEventListener("popstate", nuevaUrl, false);

	//El tercer parámetro es this
	window.history.replaceState(1, null);
};

var cambiarUrl = () => {

	mostrar(2);
	window.history.pushState(2, null, "pagina2.html");
};

var nuevaUrl = (e) => {

	mostrar(e.state);
};

var mostrar = (estado) => {

	zonaDatos.innerHTML = "Estás en la página " + estado;
};

window.addEventListener("load", main, false);