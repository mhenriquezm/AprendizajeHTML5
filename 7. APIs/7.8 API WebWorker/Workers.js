var main = () => {

	zonaDatos = document.getElementById("zona-datos");

	var boton = document.getElementById("boton");

	boton.addEventListener("click", enviar, false);

	//Creamos un objeto de tipo worker o Thread secundario
	trabajador = new Worker("Trabajador.js");

	trabajador.addEventListener("message", recibir, false);
};

var enviar = () => {

	var nombre = document.getElementById("nombre");

	//Al no especificar segundo parámetro se sobre toma this
	trabajador.postMessage(nombre.value);

	nombre.value = "";
};

var recibir = (e) => {

	zonaDatos.innerHTML = e.data;
};

window.addEventListener("load", main, false);