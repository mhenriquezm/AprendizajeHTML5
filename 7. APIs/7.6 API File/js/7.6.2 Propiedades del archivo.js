var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	var archivos = document.getElementById("archivos");

	archivos.addEventListener("change", procesar, false);
};

var procesar = (e) => {

	//Creamos un array con los elementos seleccionados por el boton
	var archivos = e.target.files;
	var seleccionado = archivos[0];

	//Vaciamos la zona de datos
	zonaDatos.innerHTML = "";

	/*Usamos la propiedad type para ver el archivo y match para 
	comprobar si es una imágen, luego negamos la condición y
	esto nos permite saber si es distinto de una imágen*/
	if(!seleccionado.type.match(/image/)) {

		alert("Selecciona una imágen");
	}
	else {
		zonaDatos.innerHTML += "Nombre del archivo: " 
			+ seleccionado.name + "<br>";

		zonaDatos.innerHTML += "Tamaño del archivo: " 
			+ (seleccionado.size / 1024).toFixed(2) + " Kb <br>";

		var lector = new FileReader();

		//Leemos el archivo y obtenemos una URL
		lector.readAsDataURL(seleccionado);

		//Al cargar la lectura incrustamos la imágen
		lector.addEventListener("load", (e) => {

			var img = new Image();
			img.src = e.target.result;

			zonaDatos.innerHTML += "<img src='" + img.src 
				+ "' width='85%'>";
		}, false);
	}
};

window.addEventListener("load", main, false);