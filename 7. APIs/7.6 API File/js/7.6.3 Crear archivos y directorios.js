//Esta API se ejecuta con mayor libertad en un servidor web
var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	var boton = document.getElementById("boton");
	var boton2 = document.getElementById("boton2");
	var mover = document.getElementById("mover");
	var eliminar = document.getElementById("eliminar");

	boton.addEventListener("click", crearArchivo, false);
	boton2.addEventListener("click", crearDirectorio, false);
	mover.addEventListener("click", modificar, false);
	eliminar.addEventListener("click", () => {

		var item = document.getElementById("item");
		var origen = ruta + item.value;

		//espacio.getFile(origen, null, (archivo) => {
		espacio.getDirectory(origen, null, (archivo) => {
			
			archivo.removeRecursively(() => {

				item.value = "";

				mostrar();

			}, errores);
		}, errores);
	}, false);

	//Solicitamos al navegador permiso para acceder al disco duro
	navigator.webkitPersistentStorage.requestQuota(
		((5 * 1024) * 1024),//Determinamos el espacio en bytes 5Mb
		acceso//Función a llamar en caso de éxito
	);
};

var acceso = () => {

	window.webkitRequestFileSystem(
		PERSISTENT,//Tipo de espacio donde se crea el sa
		((5 * 1024) * 1024),//Espacio reservado
		crearSistema,//Función a llamar en caso de éxito
		errores//Función a llamar en caso de error
	);
};

//Capturamos el objeto FileSystem
var crearSistema = (sistema) => {
	//Almacenamos la raiz del sistema de archivos
	espacio = sistema.root;
	ruta = "";

	mostrar();
};

var crearArchivo = () => {

	entrada = document.getElementById("entrada");

	var nombreArchivo = entrada.value;

	if(nombreArchivo != "") {
		//Asignamos la ruta antes del nombre
		nombreArchivo = ruta + nombreArchivo;

		espacio.getFile(
			nombreArchivo, 
			{create: true, exclusive: false},
			mostrar,
			errores
		);
	}
};

var crearDirectorio = () => {

	entrada2 = document.getElementById("entrada2");

	var nombreDirectorio = entrada2.value;

	if(nombreDirectorio != "") {
		//Asignamos la ruta antes del nombre
		nombreDirectorio = ruta + nombreDirectorio;

		espacio.getDirectory(
			nombreDirectorio, 
			{create: true, exclusive: false},
			mostrar,
			errores
		);
	}
};

var modificar = () => {
	var origen = document.getElementById("origen");
	var destino = document.getElementById("destino");

	espacio.getFile(origen.value, null, (archivo) => {

		espacio.getDirectory(destino.value, null, (directory) => {

			archivo.moveTo(directory, null, () => {

				origen.value = "";
				destino.value = "";

				mostrar();

			}, errores);
		}, errores);
	}, errores);
};

var mostrar = () => {
	//Limpiamos zonas
	entrada.value = "";
	entrada2.value = "";
	zonaDatos.innerHTML = "";

	//Abrimos el directorio, (null para leer directorio)
	espacio.getDirectory(ruta, null, leerDirectorio, errores);
};

//Crear el lector
var leerDirectorio = (directorio) => {

	lector = directorio.createReader();

	leer();
};

//Leer entradas
var leer = () => {

	lector.readEntries((archivos) => {
		//En caso de lectura exitosa
		if(archivos.length) {
			listar(archivos);
		}
	}, errores);
};

//Recorre el array de archivos
var listar = (archivos) => {

	archivos.forEach((item) => {

		if(item.isFile) {
			
			zonaDatos.innerHTML += item.name + "<br/>";
		} 
		else if(item.isDirectory) {
			
			zonaDatos.innerHTML += "" 
				+ "<span onclick='cambiarDirectorio(\"" 
				+ item.name + "\")' class='directorio'>"
					+ item.name
				+ "</span><br/>";
		}

	});

};

var cambiarDirectorio = (nuevaRuta) => {

	ruta = ruta + nuevaRuta + "/";
	mostrar();
};

var errores = (error) => {

	alert("Error al crear el archivo. " + error.code);
};

var volver = () => {

	espacio.getDirectory(ruta, null, (dirActual) => {

		dirActual.getParent((dirPadre) => {

			ruta = dirPadre.fullPath;

			mostrar();
		}, errores);

	}, errores);
};

window.addEventListener("load", main, false);