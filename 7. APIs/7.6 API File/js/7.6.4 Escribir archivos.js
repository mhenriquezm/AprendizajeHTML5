var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	var boton = document.getElementById("boton");

	boton.addEventListener("click", escribirArchivo, false);

	navigator.webkitPersistentStorage.requestQuota(
		((5 * 1024) * 1024),
		acceso
	);
};

var acceso = () => {

	window.webkitRequestFileSystem(
		PERSISTENT,
		((5 * 1024) * 1024),
		crearSistema,
		errores
	);
};

var crearSistema = (sistema) => {

	espacio = sistema.root;
};

var escribirArchivo = () => {

	entrada = document.getElementById("archivo-origen");
	var nombre = entrada.value;

	espacio.getFile(nombre, 
		{create: true, exclusive: false}, (archivo) => {

			//Creamos un objeto de escritura
			archivo.createWriter(escribirContenido, errores);
		}, errores);
};

var escribirContenido = (fileWriter) => {

	var entradaTexto = document.getElementById("texto");
	var texto = entradaTexto.value;

	//Creamos un objeto blob que recibe un array y el tipo
	var blob = new Blob([texto], {type: "text/html"});

	//Escribimos el objeto
	fileWriter.write(blob);

	//Al terminar la escritura 
	fileWriter.addEventListener("writeend", () => {

		entrada.value = "";
		entradaTexto.value = "";

		zonaDatos.innerHTML = "Archivo creado con éxito.";
	}, false);
}

var errores = (error) => {

	alert("Error al crear el archivo. " + error.code);
};

window.addEventListener("load", main, false);