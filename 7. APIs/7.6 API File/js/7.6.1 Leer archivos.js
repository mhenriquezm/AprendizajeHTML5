var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	var archivos = document.getElementById("archivos");

	archivos.addEventListener("change", procesar, false);
};

var procesar = (e) => {

	//Creamos un array con los elementos seleccionados por el boton
	var archivos = e.target.files;
	var seleccionado = archivos[0];

	//Creamos un lector de archivos
	var lector = new FileReader();

	//Leemos el archivo y lo representamos en texto utf-8
	lector.readAsText(seleccionado);

	//Hacemos que el lector desencadene el evento load
	lector.addEventListener("load", (e) => {
		
		var resultset = e.target.result;

		zonaDatos.innerHTML = resultset;
	}, false);
};

window.addEventListener("load", main, false);