var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	var boton = document.getElementById("boton");

	boton.addEventListener("click", leerArchivo, false);

	navigator.webkitPersistentStorage.requestQuota(
		((5 * 1024) * 1024),
		acceso
	);
};

var acceso = () => {

	window.webkitRequestFileSystem(
		PERSISTENT,
		((5 * 1024) * 1024),
		crearSistema,
		errores
	);
};

var crearSistema = (sistema) => {

	espacio = sistema.root;
};

var leerArchivo = () => {

	entrada = document.getElementById("archivo-origen");
	var nombre = entrada.value;

	espacio.getFile(nombre, {create: true, exclusive: false},
		(archivo) => {

			archivo.file(leerContenido, errores);
			
		}, errores);
};

var leerContenido = (File) => {

	zonaDatos.innerHTML = ""
		+ "<div>"
			+ "- Nombre: " + File.name + "<br/>"
			+ "- Tamaño: " + File.size + " bytes<br/>"
		+ "</div>";

	var lector = new FileReader();

	lector.addEventListener("load", (e) => {

		var resulset = e.target.result;
		entrada.value = "";

		zonaDatos.innerHTML += ""
			+ "<div>"
				+ "Contenido: " + resulset
			+ "</div>";
	}, false);

	lector.readAsText(File);
};

var errores = (error) => {

	alert("Error al crear el archivo. " + error.code);
};

window.addEventListener("load", main, false);