var main = () => {
	mensaje = "";
	m_valor = "";
	iframe = "";

	var boton = document.getElementById("boton");

	boton.addEventListener("click", enviar, false);

	//El documento esta a la escucha de recibir un mensaje
	window.addEventListener("message", recibir, false);

	recepcion = document.getElementById("zona-recepcion");
};

var enviar = () => {

	if(mensaje == "") {
		mensaje = document.getElementById("mensaje");
	}

	if(iframe == "") {
		iframe = document.getElementById("iframe");
	}

	m_valor = mensaje.value;

	//Enviamos un valor para el iframe al dominio del receptor
	iframe.contentWindow.postMessage(m_valor, "http://localhost");

	mensaje.value = "";
};

var recibir = (e) => {

	recepcion.innerHTML += e.data;
};

window.addEventListener("load", main, false);