var main = () => {

	zonaMensajes = document.getElementById("zona-mensajes");

	window.addEventListener("message", receptor, false);
};

var receptor = (e) => {

	if(e.origin == "http://localhost") {
		zonaMensajes.innerHTML += "Mensaje desde: " + e.origin 
		+ "<br/>" + "Mensaje: " + e.data + "<br/>";

		//Devolvemos una respuesta al origen
		e.source.postMessage("Mensaje recibido <br/>", e.origin);
	} 
	else {

		e.source.postMessage("Mensaje erróneo", e.origin);
	}
};

window.addEventListener("load", main, false);