var main = () => {

	zonaDatos = document.getElementById("zona-datos");

	var boton = document.getElementById("boton");

	boton.addEventListener("click", enviarDatos, false);
};

var enviarDatos = () => {

	/*
		1. Identificar la información a enviar
		2. Crear el objeto FormData
		3. Agregar información al objeto con el método append 
		(clave, valor)
		4. Enviar la información al servidor con el objeto XHR
	*/

	nombre = document.getElementById("nombre");
	apellido = document.getElementById("apellido");

	var el_nombre = nombre.value;
	var el_apellido = apellido.value;
	var paquete = new FormData();

	paquete.append('nombre', el_nombre);
	paquete.append('apellido', el_apellido);

	/*
		1. Almacenar la ruta del archivo php en una variable
		2. Crear el objeto XMLHttpRequest
		3. Crear los manejadores de eventos
		4. Abrir conexiones
		5. Enviar los datos
	*/

	var url = "php/procesar.php";
	var peticion = new XMLHttpRequest();

	peticion.addEventListener("load", mostrar, false);
	peticion.open("POST", url, true);
	peticion.send(paquete);

	nombre.value = "";
	apellido.value = "";
};

var mostrar = (e) => {

	zonaDatos.innerHTML = e.target.responseText;
};

window.addEventListener("load", main, false);