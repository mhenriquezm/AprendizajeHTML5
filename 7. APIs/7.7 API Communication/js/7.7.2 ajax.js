var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	zonaProgreso = document.getElementById("zona-progreso");
	barraProgreso = "";

	var boton = document.getElementById("boton");

	boton.addEventListener("click", leer, false);
};

var leer = () => {

	var url = "recursos/video-2012-04-05-14-22-32.mp4";
	var solicitud = new XMLHttpRequest();

	solicitud.addEventListener("loadstart", inicioBarra, false);
	solicitud.addEventListener("progress", estadoBarra, false);
	solicitud.addEventListener("load", mostrar, false);

	solicitud.open("GET", url, true);
	solicitud.send(null);
};

var inicioBarra = () => {

	zonaDatos.innerHTML = "<progress value='0' max='100'>" 
	+ "</progress>";
};

//Recibimos el recurso solicitado (El video)
var estadoBarra = (e) => {

	//Regla de 3 para calcular el porcentaje de descarga
	var porcentaje = parseInt(((e.loaded * 100) / e.total));
	
	if(barraProgreso == "") {
		barraProgreso = zonaDatos.querySelector("progress");
	}

	barraProgreso.value = porcentaje;//Incrementamos la barra
	zonaProgreso.innerHTML = porcentaje + " %";
};

var mostrar = (e) => {

	zonaDatos.innerHTML = "Descarga exitosa";
};

window.addEventListener("load", main, false);