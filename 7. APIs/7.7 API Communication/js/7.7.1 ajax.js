var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	var boton = document.getElementById("boton");

	boton.addEventListener("click", leer, false);
};

var leer = () => {

	var url = "recursos/texto.txt";//Ruta del recurso
	var solicitud = new XMLHttpRequest();//Objeto XHR

	//Al cargar el objeto
	solicitud.addEventListener("load", mostrar, false);

	//Abrimos una conexión pasando el método, la ruta y un booleano
	solicitud.open("GET", url, true);

	//Especificamos la información a enviar
	solicitud.send(null);
};

var mostrar = (e) => {

	//Mostramos el objeto solicitado en formato de texto
	zonaDatos.innerHTML = e.target.responseText;
};

window.addEventListener("load", main, false);