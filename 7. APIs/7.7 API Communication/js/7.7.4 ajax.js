var main = () => {

	zonaDatos = document.getElementById("zona-datos");
	zonaProgreso = document.getElementById("zona-progreso");

	var boton = document.getElementById("archivo");

	//Los botones de tipo file manejan el evento change no click
	boton.addEventListener("change", subirArchivo, false);
};

var subirArchivo = (e) => {

	//Almacenamos los archivos que se hayan subido con el botón
	var archivos = e.target.files;
	var archivo = archivos[0];

	var url = "php/procesar_archivo.php";
	var peticion = new XMLHttpRequest();

	var paquete = new FormData();

	paquete.append("archivo", archivo);

	/*Usando el atributo upload podemos llamar funciones segun 
	el estado de la subida, inicio, duracion, subida*/
	var subida = peticion.upload;
	var porcentaje = 0;
	var progreso = "";

	subida.addEventListener("loadstart", () => {

		zonaDatos.innerHTML = "<progress value='0' max='100'>" 
			+ "</progress>";

	}, false);

	subida.addEventListener("progress", (e) => {
		
		porcentaje = parseInt(((e.loaded * 100) / e.total));

		if(progreso == "") {
			progreso = zonaDatos.querySelector("progress");
		}

		progreso.value = porcentaje;
		zonaProgreso.innerHTML = porcentaje + " %";

	}, false);

	subida.addEventListener("load", (e) => {

		zonaDatos.innerHTML = "Archivo cargado exitosamente";

	}, false);

	peticion.open("POST", url, true);
	peticion.send(archivo);
};

window.addEventListener("load", main, false);