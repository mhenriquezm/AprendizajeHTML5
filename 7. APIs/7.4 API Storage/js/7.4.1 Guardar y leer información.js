var main = () => {
	var boton = document.getElementById("grabar");

	boton.addEventListener("click", nuevoItem, false);
};

var nuevoItem = () => {
	//Item = clave: valor;
	var clave = document.getElementById("clave").value;
	var valor = document.getElementById("valor").value;

	//Guardar el item mientras dure la sesión de la pestaña
	sessionStorage.setItem(clave, valor);

	//Al conocer la clave podemos acceder a su valor
	leerMostrar(clave);
};

var leerMostrar = (clave) => {
	var zonaDatos = document.getElementById("zonadatos");
	var item = sessionStorage.getItem(clave);

	zonaDatos.innerHTML = "<div>"
			+ "Clave: " + clave 
			+ "--"
			+ "Valor: " + item
		+ "</div>";
};

window.addEventListener("load", main, false);