var main = () => {
	var boton = document.getElementById("grabar");

	boton.addEventListener("click", nuevoItem, false);
};

var nuevoItem = () => {
	var clave = document.getElementById("clave");
	var valor = document.getElementById("valor");

	//Sintaxis alternativa, algo similar a los arrays asociativos
	sessionStorage[clave.value] = valor.value;

	leerMostrar(clave.value);

	//Reseteo de campos
	clave.value = "";
	valor.value = "";
};

var leerMostrar = (clave) => {
	var zonaDatos = document.getElementById("zonadatos");
	var longSesion = sessionStorage.length;
	var clave2 = "";
	var valor2 = "";

	zonaDatos.innerHTML = "<div>"
			+ "<button onclick='eliminarTodo();'>"
				+ "Eliminar todo" 
			+ "</button>" 
		+ "</div>";

	//Recorrer todas las variables de sesion
	for(var i = 0;i < longSesion; i++) {
		//Acceder a cada posición del array de sesion
		clave2 = sessionStorage.key(i);
		valor2 = sessionStorage[clave2];

		zonaDatos.innerHTML += "<div>"
				+ "Clave: " + clave2
				+ "--"
				+ "Valor: " + valor2
			+ "</div>";
	}
};

var eliminarTodo = () => {
	if(confirm("¿Estás seguro?")) {
		sessionStorage.clear();//Borramos los datos de sesión
		leerMostrar();
	}
};

window.addEventListener("load", main, false);