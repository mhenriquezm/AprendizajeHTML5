//Variables globales
var vacacion = '', reproducir = '', barra = '', progreso = '';
var maximo = 600;//Ancho total de la barra
var momento = 0;
var posX = 0;
var nuevoTiempo = 0;

//Funciones generales
function main() {
	//Identificación de elementos
	vacacion = document.getElementById('vacacion');
	reproducir = document.getElementById('reproducir');
	barra = document.getElementById('barra');
	progreso = document.getElementById('progreso');

	//Preparar las fuentes de evento
	reproducir.addEventListener('click', clicar, false);
	barra.addEventListener('click', alterar, false);
}

function clicar() {
	//Comprobar el estado del video
	if((!vacacion.paused) && (!vacacion.ended)) {
		vacacion.pause();
		reproducir.innerHTML = "Play";
	}
	else {
		vacacion.play();
		reproducir.innerHTML = "Pause";

		//Establecemos un intervalo de llamadas por segundo
		setInterval(estado, 30);
	}
}

function estado() {
	//Comprobar el estado del video
	if(!vacacion.ended) {
		//Proceso para saber en que momento del video estamos
		momento = (maximo*vacacion.currentTime)/vacacion.duration;

		//Avanzamos segun el instante
		progreso.style.width = momento.toFixed(2) + 'px';
	}
}

function alterar(e) {
	//Comprobar el estado del video
	if((!vacacion.paused) && (!vacacion.ended)) {
		//Calculamos la posicion del click
		posX = e.pageX - barra.offsetLeft;

		//Calculamos el momento durante el click
		nuevoTiempo = (posX * vacacion.duration) / maximo;

		//Enviamos el video a ese momento
		vacacion.currentTime = nuevoTiempo;

		//Enviamos la barra a ese momento
		progreso.style.width = posX.toFixed(2) + 'px';
	}
}

//El programa iniciará al cargar la ventana
window.addEventListener('load', main, false);