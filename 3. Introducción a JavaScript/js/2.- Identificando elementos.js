/*Si estamos programando en un Script externo no necesitamos las 
etiquetas <script></script>*/

function saludo() {//Cramos una función que ejecutará el saludo	
	alert("Hola mundo, este es mi primer script");	
}

function saludo2() {//Cramos una función que ejecutará el saludo2
	alert("Script importante");	
}

function pulsar() {
	/*Usamos el objeto document con el método getElementsByTagName 
	para obtener un array con todos los elementos de la etiqueta dentro 
	del argumento. Para seleccionar el correspondiente se usa la posición
	del array e inmediatamente lo ponemos a la escucha de un evento de 
	onclick llamando a la función saludo*/
	document.getElementsByTagName('p')[2].onclick=saludo;

	/*Rescatamos el Id del elemento y lo ponemos a la escucha de un evento 
	diferente al pasar el mouse*/
	document.getElementById('importante').onmouseover=saludo2;
}

/*Evento de ventana, garantiza que el código que llama se ejecutará luego 
de cargar toda la página*/
window.onload=pulsar;