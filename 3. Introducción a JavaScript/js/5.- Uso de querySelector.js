//Poner a la escucha al último elemento con los estilos CSS

function saludo() {//Cramos una función que ejecutará el saludo	
	alert("Hola mundo, este es mi primer script");	
}

function pulsar() {
	//querySelector apunta con selectores CSS y nos devolverá el primero que cumpla con el criterio
	document.querySelector("#principal p.importante:last-child").onclick=saludo;
}

//Evento de ventana, garantiza que el código que llama se ejecutará luego de cargar toda la página
window.onload=pulsar;