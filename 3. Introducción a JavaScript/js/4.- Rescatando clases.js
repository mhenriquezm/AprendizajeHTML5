//Si estamos programando en un Script externo no necesitamos las etiquetas <script></script>

function saludo() {//Cramos una función que ejecutará el saludo	
	alert("Hola mundo, este es mi primer script");	
}

function pulsar() {
	//También rescatamos un array y hay que almacenarlo en una variable
	var array=document.getElementsByClassName('importante'),
		longitud=array.length;

	for(var i=0;i<longitud;i++){
		//Ponemos a cada elemento a la escucha
		array[i].onclick=saludo;
	}
}

//Evento de ventana, garantiza que el código que llama se ejecutará luego de cargar toda la página
window.onload=pulsar;