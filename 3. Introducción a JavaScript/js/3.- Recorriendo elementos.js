//Si estamos programando en un Script externo no necesitamos las etiquetas <script></script>

function saludo() {//Cramos una función que ejecutará el saludo	
	alert("Hola mundo, este es mi primer script");	
}

function pulsar() {
	var arreglo=document.getElementsByTagName('p'),//Rescatamos los elementos <p> en un arreglo
		longitud=arreglo.length;//Obtenemos la longitud del arreglo

	//Como estamos obteniendo un array con las etiquetas podemos recorrerlo con un bucle
	for(var i=0;i<longitud;i++){
		arreglo[i].onclick=saludo;
	}
}

//Evento de ventana, garantiza que el código que llama se ejecutará luego de cargar toda la página
window.onload=pulsar;