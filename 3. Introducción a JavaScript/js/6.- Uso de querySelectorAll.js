//Poner a la escucha a todos los párrafos dentro del div con id principal y a la palabra séptimo

function saludo() {//Cramos una función que ejecutará el saludo	
	alert("Hola mundo, este es mi primer script");	
}

function pulsar() {
	//querySelectorAll devuelve un array con todos los elementos que cumplan con el selector CSS
	var array=document.querySelectorAll("#principal p, .destacar"),
		longitud=array.length;

	for(var i=0;i<longitud;i++){
		//Ponemos a todos los elementos del array a la escucha
		array[i].onclick=saludo;
	}
}

//Evento de ventana, garantiza que el código que llama se ejecutará luego de cargar toda la página
window.onload=pulsar;