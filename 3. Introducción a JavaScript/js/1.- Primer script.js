/*Si estamos programando en un Script externo no necesitamos las etiquetas 
<script></script>*/

function saludo() {//Cramos una función que ejecutará el saludo	
	alert("Hola mundo, este es mi primer script");	
}

function pulsar() {
	/*Usamos el objeto document con el método getElementsByTagName para 
	obtener un array con todos los elementos de la etiqueta dentro del 
	argumento. Para seleccionar el correspondiente se usa la posición
	del array e inmediatamente lo ponemos a la escucha de un evento 
	de click llamando a la función saludo*/
	document.getElementsByTagName('p')[0].
	addEventListener('click',saludo,false);
}

/*Evento de ventana, garantiza que el código que llama se ejecutará 
luego de cargar toda la página, usamos el objeto correspondiente y
accedemos al método addEventListener, este método crea un objeto oyente
y recibe 3 parámetros, evento, funcion a ejecutar y un booleano que indica
si da prioridad a esa funcion por tener otras funciones anidadas*/
window.addEventListener('load',pulsar,true);