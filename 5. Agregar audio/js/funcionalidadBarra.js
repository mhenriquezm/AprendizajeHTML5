//Variables globales
var miAudio = '', reproducir = '', barra = '', progreso = '';
var maximo = 600;//Ancho total de la barra
var momento = 0;
var posX = 0;
var nuevoTiempo = 0;

//Funciones generales
function main() {
	//Identificación de elementos
	miAudio = document.getElementById('miAudio');
	reproducir = document.getElementById('play');
	barra = document.getElementById('barra');
	progreso = document.getElementById('progreso');

	//Preparar las fuentes de evento
	reproducir.addEventListener('click', clicar, false);
	barra.addEventListener('click', alterar, false);
}

function clicar() {
	//Comprobar el estado del video
	if((!miAudio.paused) && (!miAudio.ended)) {
		miAudio.pause();
		reproducir.innerHTML = "Play";
	}
	else {
		miAudio.play();
		reproducir.innerHTML = "Pause";

		//Establecemos un intervalo de llamadas por segundo
		setInterval(estado, 30);
	}
}

function estado() {
	//Comprobar el estado del video
	if(!miAudio.ended) {
		//Proceso para saber en que momento del video estamos
		momento = (maximo*miAudio.currentTime)/miAudio.duration;

		//Avanzamos segun el instante
		progreso.style.width = momento.toFixed(2) + 'px';
	}
}

function alterar(e) {
	//Comprobar el estado del video
	if((!miAudio.paused) && (!miAudio.ended)) {
		//Calculamos la posicion del click
		posX = e.pageX - barra.offsetLeft;

		//Calculamos el momento durante el click
		nuevoTiempo = (posX * miAudio.duration) / maximo;

		//Enviamos el video a ese momento
		miAudio.currentTime = nuevoTiempo;

		//Enviamos la barra a ese momento
		progreso.style.width = posX.toFixed(2) + 'px';
	}
}

//El programa iniciará al cargar la ventana
window.addEventListener('load', main, false);